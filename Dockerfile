#this is a dockerfile to create image for python script to fetch weather info from openWeatherAPI
#IT accepts user input and output using google text to speech.

FROM python:3.9
ADD openWeatherApi.py .
RUN pip install requests gtts playsound datetime
CMD [ "python", "./openWeatherApi.py" ]
