import requests
import os
from datetime import datetime
from gtts import gTTS
from playsound import playsound

language = 'en'


api_key = '42440f4477df938ae11483f074a8cdfd'

city = input('Enter The Name Of The City: ')


response = requests.get(f'http://api.openweathermap.org/data/2.5/weather?q={city}&appid={api_key}&units=metric')


data = response.json()


#required Data from the resposne
temp = str(data['main']['temp'])
wind_speed = data['wind']['speed']
humidity = data['main']['humidity']
description = data['weather'][0]['description']
latitude = data['coord']['lat']
longitude = data['coord']['lon']




output =(
f'The current weather for {city} is {description}. with temperature {temp} degree Celsius\n'
f'humidity, is {humidity} %.with a wind speed of {wind_speed} km/h.'
)

print(output)
myobj = gTTS(text=output, lang=language, slow=False) 
myobj.save("welcome.mp3")
playsound('welcome.mp3')